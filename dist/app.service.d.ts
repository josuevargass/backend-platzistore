import { ConfigType } from '@nestjs/config';
import { Client } from "pg";
import config from './config';
export declare class AppService {
    private clientePg;
    private tasks;
    private configService;
    constructor(clientePg: Client, tasks: any[], configService: ConfigType<typeof config>);
    getHello(): string;
}
