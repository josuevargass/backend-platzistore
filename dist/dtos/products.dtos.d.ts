export declare class CreateProductDto {
    readonly name: string;
    readonly price: number;
    readonly description: string;
    readonly stock: number;
    readonly image: string;
}
declare const UpdateProductDto_base: import("@nestjs/common").Type<Partial<CreateProductDto>>;
export declare class UpdateProductDto extends UpdateProductDto_base {
    readonly name: string;
}
export {};
