declare const myName = "Josue";
declare const myAge = 12;
declare const suma: (a: number, b: number) => number;
declare class Persona {
    private age;
    private name;
    constructor(age: number, name: string);
    getSummary(): string;
}
declare const Josue: Persona;
