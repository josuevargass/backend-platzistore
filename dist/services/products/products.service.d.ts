import { Product } from 'src/Entitys/product.entity';
import { CreateProductDto, UpdateProductDto } from '../../dtos/products.dto';
export declare class ProductsService {
    private counterId;
    private products;
    findAll(): Product[];
    findOne(id: number): Product;
    create(payload: CreateProductDto): {
        name: string;
        price: number;
        description: string;
        stock: number;
        image: string;
        id: number;
    };
    update(id: number, payload: UpdateProductDto): Product;
    remove(id: number): boolean;
}
