import { ConfigService } from '@nestjs/config';
import { Client } from "pg";
import { User } from '../entities/user.entity';
import { CreateUserDto, UpdateUserDto } from '../dtos/user.dto';
import { ProductsService } from './../../products/services/products.service';
export declare class UsersService {
    private productsService;
    private configService;
    private clientePg;
    constructor(productsService: ProductsService, configService: ConfigService, clientePg: Client);
    private counterId;
    private users;
    findAll(): User[];
    findOne(id: number): User;
    create(data: CreateUserDto): {
        email: string;
        password: string;
        role: string;
        id: number;
    };
    update(id: number, changes: UpdateUserDto): User;
    remove(id: number): boolean;
    getOrderByUser(id: number): Promise<{
        date: Date;
        user: User;
        products: import("../../products/entities/product.entity").Product[];
    }>;
    getTask(): Promise<unknown>;
}
