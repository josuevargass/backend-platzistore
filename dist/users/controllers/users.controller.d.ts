import { UsersService } from '../services/users.service';
import { CreateUserDto, UpdateUserDto } from '../dtos/user.dto';
export declare class UsersController {
    private usersService;
    constructor(usersService: UsersService);
    findAll(): import("../entities/user.entity").User[];
    tasks(): Promise<unknown>;
    get(id: number): import("../entities/user.entity").User;
    getOrders(id: number): Promise<{
        date: Date;
        user: import("../entities/user.entity").User;
        products: import("../../products/entities/product.entity").Product[];
    }>;
    create(payload: CreateUserDto): {
        email: string;
        password: string;
        role: string;
        id: number;
    };
    update(id: number, payload: UpdateUserDto): import("../entities/user.entity").User;
    remove(id: number): boolean;
}
