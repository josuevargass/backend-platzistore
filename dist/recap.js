const myName = 'Josue';
const myAge = 12;
const suma = (a, b) => {
    return a + b;
};
suma(12, 12);
class Persona {
    constructor(age, name) {
        this.age = age;
        this.name = name;
    }
    getSummary() {
        return `My name is ${this.name} and my age is ${this.age} years old`;
    }
}
const Josue = new Persona(23, 'Josue');
Josue.getSummary();
//# sourceMappingURL=recap.js.map