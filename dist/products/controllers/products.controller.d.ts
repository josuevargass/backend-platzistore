import { ProductsService } from './../services/products.service';
export declare class ProductsController {
    private productsService;
    constructor(productsService: ProductsService);
    getProducts(): Promise<import("../entities/product.entity").Product[]>;
    getProductFilter(): string;
    getOne(productId: number): Promise<import("../entities/product.entity").Product>;
}
