import { ProductsService } from 'src/services/products/products.service';
import { CreateProductDto, UpdateProductDto } from '../../dtos/products.dto';
export declare class ProductController {
    private productsService;
    constructor(productsService: ProductsService);
    getProducts(limit: number, offset: number, brand: string): any;
    getProduct(id: number): any;
    getProductFilter(): {
        result: string;
    };
    create(payload: CreateProductDto): any;
    update(id: number, payload: UpdateProductDto): any;
    delete(id: number): any;
}
