export declare class CategoryController {
    getCategory(id: number, productId: number): string;
}
