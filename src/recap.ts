const myName = 'Josue';

const myAge = 12;

const suma = (a: number, b: number) => {
  return a + b;
};

suma(12, 12);

class Persona {
  // private age:number;
  // private name:string;

  constructor(private age: number, private name: string) {
    // this.age = age;
    // this.name = name;
  }

  getSummary() {
    return `My name is ${this.name} and my age is ${this.age} years old`;
  }
}

const Josue = new Persona(23, 'Josue');

Josue.getSummary();
