import { Injectable, Inject } from '@nestjs/common';
import { ConfigType } from '@nestjs/config';
import { Client } from "pg";

import config from './config';

@Injectable()
export class AppService {
  constructor(
    @Inject('PG') private clientePg: Client,
    // private configService: ConfigService,
    @Inject('TASKS') private tasks: any[],
    @Inject(config.KEY) private configService: ConfigType<typeof config>
  ) {}

  getHello(): string {
    const apiKey = this.configService.apiKey;
    const name = this.configService.database.name;
    return `Hello World! Josue ${apiKey} ${name}`;
  }

}
